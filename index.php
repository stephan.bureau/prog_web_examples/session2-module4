<html>

<head>
  <base href="/" />
  <title>Ceci est mon titre</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="styles/style.css" />
  <link rel="shortcut icon" href="assets/html5.png">
</head>

<body>
  <h1>Exemples</h1>
  <?php
  include_once("src/models/contact.php");

  // créer un contact
  $contact = new Contact("Josh");
  ?>
  <h2>demo 1 - Create a contact and display contacts list</h2>
  <h3>Instanciation d'une classe + Utilisation de ses méthodes</h3>
  <table style="background-color: #fc7; border-radius: 10px; padding: 10px;">
    <th>Contact card:</th>
    <tr>
      <td>Name: <?php echo $contact->getName() ?></td>
    </tr>
    <tr>
      <td>Age: <?php echo $contact->getAge() ?> years old</td>
    </tr>
  </table>

  <br><br>

  <h3>Instanciation de plusieurs classes + Combinaison de leurs méthodes pour stocker des instances "in-memory"</h3>

  <?php
  include_once("src/models/adressBook.php");

  // créer un agenda d'adresses
  $adressBook = new AdressBook();

  // ajout d'un contact à l'agenda


  // echo sha1("exemple");
  $contact = new Contact("Josh");
  // $contact->age = 37;
  // $contact->setAge(36);

  $adressBook->addContact($contact);

  $adressBook->addContact(new Contact("Magaly"));
  // array_push($adressBook->contactsList, $contact);

  $contacts =  $adressBook->getContactsList();
  ?>

  <label>Contacts list:</label>
  <ul>
    <?php
    foreach ($contacts as $key => $contact) {
      echo "<li>" . $contact->getName() . " - " . $contact->getAge() . "</li>";
    }
    ?>
  </ul>



  <hr>
  <h2>demo 2 - Get specific contact</h2>
  <h3>Réutilisation d'une instance + Utilisation d'une méthode pour récupérer des données "in-memory"</h3>
  <?php
  $contact = $adressBook->getContactFromContactName("Magaly");
  // echo "test:" . array_search("Magaly", array_column($adressBook->contactsList, "name"));
  echo $contact->getName() . "'s age is " . $contact->getAge();
  ?>


  <hr>
  <h2>demo 3 - Create a friend</h2>
  <h3>Instanciation et réutilisation d'instances + Héritage de classe + Récupération des données "in-memory"</h3>
  <?php
  include_once("src/models/friend.php");
  $friend = new Friend("Alex", 29, "23th street");
  $friend->phone = "555-1234-567";

  $adressBook->addContact($friend);

  echo $friend->getName() . "'s age is " . $friend->getAge() . " and phone is " . $friend->phone;

  ?>

  <label>Contacts list:</label>
  <ul>
    <?php
    $contacts =  $adressBook->getContactsList();
    foreach ($contacts as $key => $contact) {
      echo "<li>" . $contact->getName() . " - " . $contact->getAge() . "</li>";
    }
    ?>
  </ul>

  <hr>

  <h2>demo 4 - Create a friend from a contact</h2>
  <h3>Instanciation par méthode statique + Instance vs tatic + Affichage des données "in-memory"</h3>

  <?php
  include_once("src/models/friend.php");
  Friend::$city = "Montreal";
  $friend = Friend::FromContact($contact);
  $friend->phone = "555-9876-123";
  echo $friend->getName() . "'s age is " . $friend->getAge() . " and phone is " . $friend->phone;
  ?>
  <p>All my friends live in <?php echo Friend::$city; ?></p>

  <hr>

  <h2 name="demo5">demo 5 - Variables de sessions PHP</h2>
  <h3>Définir et réutiliser les sessions PHP</h3>

  <?php
  session_start();

  define("EXPIRATION_TIME", 10);

  function generateSession()
  {
    $_SESSION['DateExpirationSession'] = time();
    $_SESSION["Data"] = "session " . time();
  }

  if (!isset($_SESSION['DateExpirationSession'])) {
    generateSession();
  } else if (time() - $_SESSION['DateExpirationSession'] > EXPIRATION_TIME) {
    // si la session a été créée il y a plus de 1 minute, on regenere une valeur
    generateSession();
  } else {
    $_SESSION['DateExpirationSession'] = time();
  }
  ?>
  <input type="text" value="<?php echo $_SESSION["Data"] ?>">
</body>

</html>