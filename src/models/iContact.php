<?php
interface IContact
{
  public function getName(): string;
  public function getAge(): int;
  public function setAge(int $age): void;
  public function getAdress(): string;
}
