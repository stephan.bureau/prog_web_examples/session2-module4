<?php
include_once("iContact.php");
class Friend extends Contact implements IContact
{
  public $phone;
  static public $city;

  public function __construct(string $name, int $age, string $address)
  {
    parent::__construct($name, $age, $address);
  }

  public static function FromContact(IContact $contact)
  {
    return new self($contact->name, $contact->age, $contact->address);
  }

  public function setInformation(string $name, int $age, string $adress, string $phone = null)
  {
    $this->name = $name;
    $this->age = $age;
    $this->address = $adress;
    $this->phone = $phone;
  }
}
