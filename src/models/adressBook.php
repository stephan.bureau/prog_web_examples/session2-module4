<?php
include_once("contact.php");
class AdressBook
{
  public $contactsList;

  private function getContactIndexFromContactId(string $contactId): string
  {
    return array_search($contactId, array_column($this->contactsList, "id"));
  }

  public function getContactIndexFromContactName(string $contactName): string
  {
    return array_search($contactName, array_column($this->contactsList, "name"));
  }

  public function __construct()
  {
    $this->contactsList = [];
  }

  public function addContact(IContact $contact): void
  {
    if ($contact->getAge() > 21) {
      $contact->isOkToGoToCasino = true;
    }
    array_push($this->contactsList, $contact);
    // $this->contactsList[] = $contact;
  }

  public function getContactsList(): array
  {
    return $this->contactsList;
  }

  public function getContactFromContactId(string $contactId): IContact
  {
    $contactIndex = $this->getContactIndexFromContactId($contactId);
    return $this->contactsList[$contactIndex];
  }

  public function getContactFromContactName(string $contactName): IContact
  {
    $contactIndex = $this->getContactIndexFromContactName($contactName);
    return $this->contactsList[$contactIndex];
  }

  public function removeContact(IContact $contact): void
  {
    $indexToDelete = $this->getContactIndexFromContactId($contact->id);
    array_splice($this->contactsList, $indexToDelete, 1);
  }
}
