<?php
include_once("iContact.php");
class Contact implements IContact
{
  public $id;
  public $name;
  protected $age;
  protected $adress;

  public function __construct(string $name, int $age = 42, string $adress = "")
  {
    $this->id = sha1($name);
    $this->setInformation($name, $age, $adress);
  }

  public function setInformation(string $name, int $age, string $adress)
  {
    $this->name = $name;
    $this->age = $age;
    $this->address = $adress;
  }

  public function getName(): string
  {
    return $this->name;
  }

  public function getAge(): int
  {
    return $this->age;
  }

  public function setAge(int $age): void
  {
    $this->age = $age;
  }

  public function getAdress(): string
  {
    return $this->adress;
  }
}
